import { Module } from '@nestjs/common'
import { PagesController } from './pages/pages.controller'
import { UsersModule } from './users/users.module'
import { PostsModule } from './posts/posts.module'

@Module({
    imports: [UsersModule, PostsModule],
    controllers: [PagesController],
    providers: [],
    exports: []
})
export class AppModule {}
