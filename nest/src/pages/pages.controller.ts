import { Controller, Get } from '@nestjs/common'

@Controller()
export class PagesController {
    @Get()
    getHome(): object {
        return { message: 'Pages controller' }
    }
}
