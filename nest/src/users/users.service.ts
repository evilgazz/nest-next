import { Injectable } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map } from 'rxjs'

@Injectable()
export class UsersService {
    private readonly API_URL = 'https://jsonplaceholder.typicode.com/users'

    constructor(private readonly httpService: HttpService) {}

    findAll() {
        return this.httpService
            .get(this.API_URL)
            .pipe(map((response) => response.data))
    }
}
